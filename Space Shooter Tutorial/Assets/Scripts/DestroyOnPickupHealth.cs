﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnPickupHealth : MonoBehaviour
{
    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
        if (gameControllerObject == null)
        {
            Debug.Log("Cannot find 'GameController' script. Look in DestroyOnPickupHealth Start Function");
        }
    }

        void onTriggerEnter(Collider other)
    {

        if (other.tag == "Boundary")
        {
            return; //synonymous with passing over the if statement
        }
        if (other.tag == "Player")
        {
            Destroy(gameObject);
            gameController.IncreaseHealth();
            gameController.UpdateHealth();
        }
    }
}