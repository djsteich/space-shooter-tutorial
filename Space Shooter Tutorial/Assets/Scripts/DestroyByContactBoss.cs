﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DestroyByContactBoss : MonoBehaviour {

    public GameObject explosion;
    public GameObject explosionSilent;
    public GameObject playerExplosion;
    public int scoreValue; //how much killing these objects is worth in points
    private GameController gameController; //variable to hold our reference to the GameController instance
    private int bossHealth;
    private int bossesDestroyed;

    void OnTriggerEnter(Collider other)
    {
        PlayerController PC = other.gameObject.GetComponent<PlayerController>(); //explain logic/what's going on here || creating an instance?


        if (other.tag == "Boundary")
        {
            return; //synonymous with passing over the if statement
        }

        if (other.tag == "Enemy")
        {
            return;
        }


        if (other.tag == "Player" && PlayerController.health >= 1)
        {
            PC.transform.DOShakePosition(.5f, new Vector3(2, 0, 2), 20, 45, false, true);

            Instantiate(explosion, transform.position, transform.rotation);
            PlayerController.health = 0;
            //Debug.Log(PlayerController.health);
            gameController.UpdateHealth();
            //Destroy(gameObject);
            if (PlayerController.health == 0)
            {
                Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
                gameController.GameOver();
                //Destroy(other.gameObject); //destroy other object
                Destroy(gameObject); //destroy self
            }

        }

        if (other.tag == "Projectile" && gameController.gameOver == false)
        {
            Instantiate(explosionSilent, transform.position, transform.rotation);
            Destroy(other.gameObject); //destroy projectile
            bossHealth -= PlayerController.persistentDamage;
            if (bossHealth <= 0)
            {
                Instantiate(explosion, transform.position, transform.rotation);
                gameController.AddScore(scoreValue);
                Destroy(other.gameObject); //destroy projectile object
                Destroy(gameObject); //destroy self
                bossesDestroyed++;
            }

        }

        if (other.tag == "Missile" & gameController.gameOver == false)
        {
            Instantiate(explosionSilent, transform.position, transform.rotation);
            //Destroy(other.gameObject);
            bossHealth -= 3;
            if (bossHealth <= 0)
            {
                Instantiate(explosion, transform.position, transform.rotation);
                Destroy(other.gameObject);
                Destroy(gameObject);
                bossesDestroyed++;
            }

        }


    }

    
    void Start()
    {
       
        bossHealth = 70;
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
        if (gameControllerObject == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }
    void Update()
    {
        if (gameController.gameOver)
        {
            Destroy(gameObject);
        }
        

        /*if (gameController.bossCount == (bossesDestroyed -1))
        {
          
            gameController.upgradeTokensText.text = "Boss Wave defeated.\nUpgrade Token aquired";
            gameController.upgradeTokens++;
        }*/
        
    }
}
