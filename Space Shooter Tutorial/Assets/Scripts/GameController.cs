﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public Vector3 spawnValues;
    public GameObject [] hazards;
    public GameObject fireRatePowerUp;
    public GameObject spreadPowerUp;
    public GameObject missilePowerUp;
    public GameObject boss;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText; //references to our text objects
    public Text waveText;
    public Text restartText;
    public Text gameOverText;
    public Text controlsText;
    public Text startGameText;
    public Text upgradeTokensText;
    public Text tokensGained;
    public Text healthText;
    public Text persistentHealthText;
    public Text persistentDamageText;
    public Text healthCostText;
    public Text damageCostText;
    public Text missileAvailableText;
  

    private bool restart;
    public bool gameOver; //if these are true, end the game/restart
    //private bool isGameStarted = false;
    private int score;
    private int wave;
    public int upgradeTokens;
    public int bossCount = 1;
    void Start()
    {
        scoreText.transform.position = new Vector3(-60f, 840f, 0f);
        waveText.transform.position = new Vector3(-60f, 800f, 0f);
        healthText.transform.position = new Vector3(-60f, 760f, 0f); //setting position to left of screen before sliding it into view

        controlsText.transform.position = new Vector3(300f, 1000f,0f); //setting position to above the screen
        controlsText.transform.DOMove(new Vector3(300f, 450f, 0f), .5f, false);

        startGameText.transform.position = new Vector3(300f,1100f, 0f);
        startGameText.transform.DOMove(new Vector3(300f, 630f, 0f), 1.5f, false);
        //scoreText.DOFade(0f, 3);
        controlsText.text = "Controls:\nLEFT CLICK to fire Primary Guns\nRIGHT CLICK to fire Secondary Guns\nWASD to move";
        startGameText.text = "Press 'Enter' to start";
        upgradeTokens = 1;
        upgradeTokensText.text = "";
        tokensGained.text = "";
        restart = false;
        gameOver = false;
        gameOverText.text = "";
        restartText.text = ""; //effectively turned off - not displaying anything
        healthText.text = "Health " + PlayerController.health;
        persistentHealthText.text = "";
        persistentDamageText.text = "";
        healthCostText.text = "";
        damageCostText.text = "";
        missileAvailableText.text = "";

        score = 0;
        wave = 1;
        UpdateScore(); //update the score at the beginning of the game after we have set its value to 0
        UpdateWave(); //update wave at the beginning of the game
       
        

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))//add a boolean to make sure you can't press enter when the game is already running isGameStarted
        //if (Input.GetButton("Submit")) //this is supposed to work? according to Will
        {
            scoreText.transform.DOMove(new Vector3(60f, 840f, 0f), .5f, false);
            waveText.transform.DOMove(new Vector3(60f, 800f, 0f), .5f, false);
            healthText.transform.DOMove(new Vector3(60f, 760f, 0f), .5f, false);
            StartCoroutine(SpawnWaves());
            startGameText.DOFade(0f, 2);
            controlsText.DOFade(0f, 2);
            upgradeTokensText.text = "";
            persistentHealthText.text = "";
            persistentDamageText.text = "";
            missileAvailableText.text = "";
            StartCoroutine(SpawnFireRatePowerUp());
            StartCoroutine(SpawnSpreadPowerUp());
            StartCoroutine(SpawnMissilePowerUp());
            PlayerController.health = PlayerController.persistentHealth;
            UpdateHealth();
            //isGameStarted = true;

        }
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("restart initiated");
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        if (gameOver && Input.GetKeyDown(KeyCode.Return))
        {
            wave = 1;
            score = 0;
            bossCount = 1;
            hazardCount = 15;
            UpdateWave();
            UpdateScore();
            PlayerController.health = PlayerController.persistentHealth;
            UpdateHealth();
            persistentHealthText.text = "";
            persistentDamageText.text = "";
            gameOver = false;
            gameOverText.text = "";
            healthCostText.text = "";
            damageCostText.text = "";

        }

    }




    IEnumerator SpawnFireRatePowerUp()
    {
        while (true) //while the number is even
        {
            //yield return new WaitForSeconds();
            //int randNum = Random.Range(1, 10);
            //Debug.Log("Random Number Created: " + randNum);
            //if (randNum > 5)
            //{
            //Debug.Log(randNum);
            yield return new WaitForSeconds(Random.Range(30, 50));

            Debug.Log("FireRate power up spawned! at wave" + wave);

            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(fireRatePowerUp, spawnPosition, spawnRotation);//creates a new instance from FireRatePowerUp prefab
                                                                       //yield return new WaitForSeconds(10); //this will set a delay between spawning Fire Rate power ups




            //}
            if (gameOver)
            {
                break;
            }
            //only need to break when you want it to end completely
        }
    }
    IEnumerator SpawnSpreadPowerUp()
    {

        while (true)
        {
            yield return new WaitForSeconds(Random.Range(15, 30));
            Debug.Log("SPREAD power up spawned! at wave" + wave);

            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(spreadPowerUp, spawnPosition, spawnRotation);//creates a new instance from SpreadPowerUp prefab
                                                                     //yield return new WaitForSeconds(10); //this will set a delay between spawning Spread power ups

            if (gameOver)
            {
                break;
            }
        }
    }

    IEnumerator SpawnMissilePowerUp()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(40, 60));
            Debug.Log("MISSILE power up spawned! at wave" + wave);

            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(missilePowerUp, spawnPosition, spawnRotation);//creates a new instance from MissilePowerUp prefab


            if (gameOver)
            {
                break;
            }
        }


    }

    IEnumerator SpawnWaves()
    {

        yield return new WaitForSeconds(startWait); //this will set a delay before the game starts
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait); //this will set a delay between spawning asteroids


            }



            yield return new WaitForSeconds(waveWait);
            wave++;
            UpdateWave();
            hazardCount += 2;
            Debug.Log("hazardCount set to " + hazardCount);
            if (wave % 5 == 0) //spawn Boss
            {
                for (int i = 0; i < bossCount; i++)
                {
                    Vector3 spawnPosition = new Vector3(0.0f, spawnValues.y, spawnValues.z);
                    Quaternion spawnRotation = Quaternion.identity;
                    Instantiate(boss, spawnPosition, spawnRotation);
                    yield return new WaitForSeconds(15); //this will set a delay between spawning boss and asteroids that come afterwards
                }
                if (wave % 5 == 0 && gameOver == false)
                {
                    bossCount++;
                    upgradeTokens++;
                    //Debug.Log(upgradeTokens);
                    tokensGained.text = "Survived " + wave + " waves. Upgrade Token Acquired";
                }

                if (gameOver == false)
                {
                    yield return new WaitForSeconds(3);
                    tokensGained.text = "";

                }
            }

            if (gameOver)
            {
                //restartText.text = "Press 'R' to Restart";
                //restart = true;
                break;
            }
        }
    }

    
    
    public void AddScore(int newScoreValue)
    {
        score += newScoreValue; //add old value and new score
        UpdateScore(); //update score and display to screen
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
    
    void UpdateWave()
    {
        //Debug.Log("wave updated");
        waveText.text = "Wave: " + wave;
        
    }

    public void UpdateTokens()
    {
        upgradeTokensText.text = "Upgrade Tokens: " + upgradeTokens;
    }

    public void UpdatePersistentHealth()
    {
        persistentHealthText.text = "Health Level " + PlayerController.persistentHealth + "/10";
    }

    public void UpdateHealth()
    {
        healthText.text = "Health: " + PlayerController.health;
    }

    public void UpdatePersistentDamage()
    {
        persistentDamageText.text = "Damage Level " + PlayerController.persistentDamage + "/3";
    }
    public void IncreaseHealth()
    {
        if (upgradeTokens > 0 && PlayerController.persistentHealth <10)
        {
            upgradeTokens -= 1;
            UpdateTokens();
            //PlayerController.health += 1;
            PlayerController.persistentHealth ++;
            //UpdateHealth();
            UpdatePersistentHealth();
        }
    }

    public void IncreaseDamage()
    {
        if (upgradeTokens>=3 && PlayerController.persistentDamage <3)
        {
            upgradeTokens -= 3;
            UpdateTokens();
            PlayerController.persistentDamage++;
            UpdatePersistentDamage();

        }
    }

    public void GameOver() //a function that we can call outside of this script
    {
        gameOverText.text = "Health Depleted!\nUpgrade your ship or\nPress 'Enter' to try again.";
        gameOver = true;
        UpdateTokens();
        UpdatePersistentHealth();
        UpdatePersistentDamage();
        healthCostText.text = "Cost: 1";
        damageCostText.text = "Cost: 3";
        StopAllCoroutines();
    }
    
}
