﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnPickUpMissile : MonoBehaviour
{
    private GameController gameController;
    
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
    }
    void OnTriggerEnter(Collider other)
    {
        PlayerController PC = other.gameObject.GetComponent<PlayerController>(); //explain logic/what's going on here || creating an instance?
        if (other.tag == "Boundary")
        {
            return; //synonymous with passing over the if statement
        }
        if (other.tag == "Player" && gameController.gameOver == false)
        {
            Debug.Log("Missile available");
            PC.allowMissile();
            Destroy(gameObject);

        }
    }
    
}
