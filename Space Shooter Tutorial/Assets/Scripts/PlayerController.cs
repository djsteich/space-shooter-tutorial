﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable] //allows the properties created in the Boundary class to be visible in Unity editor
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

    public Boundary boundary; //custom class to hold boundary values accessible to any object, not just player
    public float speed; //custom property for player
    public float tilt; //custom property to tilt when moving left/right'
    public static int health; //custom property to allow the player to take hits from asteroids
    public static int persistentHealth = 1;
    public static int persistentDamage = 1;

    public static float fireRate = .30f;
    public static float missileFireRate = .50f;
    private float nextFire;
    private float nextMissileFire;
    private bool canShootSpread = false;
    private bool canShootMissile = false;


    public GameObject shot;
    public GameObject missile;
    public Transform shotSpawn; //to help instantiate the shots the player shoots
    public Transform leftShotSpawn;
    public Transform rightShotSpawn;
    public Transform missileSpawn;

    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
        if (gameControllerObject == null)
        {
            Debug.Log("Cannot find 'GameController' script. Look in PlayerController Start Function");
        }
        health = 1;
        gameController.UpdateHealth();
       
        
        //Debug.Log("PlayerController health set to 3");
        
    }

    void FixedUpdate()
    {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
        
            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            GetComponent<Rigidbody>().velocity = movement * speed;
            GetComponent<Rigidbody>().position = new Vector3
            (
                Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
                0.0f,
                Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
            ); //bounds the player to edges of screen somehow

            GetComponent<Rigidbody>().rotation = Quaternion.Euler(0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
        
    }

    void Update()
    {
        if (Input.GetButton ("Fire1") && Time.time > nextFire && gameController.gameOver== false) //shoot normal projectile
        {
            GetComponent<AudioSource>().Play();
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            if (canShootSpread)
            {
                Instantiate(shot, leftShotSpawn.position, leftShotSpawn.rotation);
                Instantiate(shot, rightShotSpawn.position, rightShotSpawn.rotation);
            }
        }
        if (canShootMissile && Input.GetButton("Fire2") && Time.time > nextMissileFire && gameController.gameOver == false) //shoot missile

        {
            GetComponent<AudioSource>().Play();
            nextMissileFire = Time.time + missileFireRate;
            Instantiate(missile, missileSpawn.position, missileSpawn.rotation);
            
        }


    }
    public void IncreaseFireRate()
    {
        fireRate = .175f;
        StartCoroutine(ResetFireRate());
    }

    IEnumerator ResetFireRate()
    {
        yield return new WaitForSeconds(8);
        fireRate = .30f;
    }

    public void ShootSpread()
    {
        canShootSpread = true;
        StartCoroutine(ResetShotSpread());
    }


    IEnumerator ResetShotSpread()
    {
        yield return new WaitForSeconds(8);
        canShootSpread = false;
    }
    public void allowMissile()
    {
        canShootMissile = true;
        gameController.missileAvailableText.text = "Missiles Available!\nRight Click to fire";
        StartCoroutine(ResetMissile());
    }

    IEnumerator ResetMissile()
    {
        yield return new WaitForSeconds(3);
        gameController.missileAvailableText.text = "";
        yield return new WaitForSeconds(9);
        canShootMissile = false;
       
    }
}
