﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    public float delay;

    private AudioSource audiosource; 

	void Start () {
        audiosource = GetComponent<AudioSource>();
        InvokeRepeating("FireWeapons", delay, fireRate);
	}

    void FireWeapons()
    {
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        audiosource.Play();
    }
	
	
}
