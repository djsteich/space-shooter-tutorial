﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAppear : MonoBehaviour {

    // Use this for initialization
    private GameController gameController;

    public GameObject menu;
    public bool isShowing;
	void Start ()
    {
        isShowing = false;
        menu.SetActive(isShowing);
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Return))
        {
            isShowing = false;
            menu.SetActive(isShowing);
            
        }
        if (gameController.gameOver)
        {

            isShowing = true;
            menu.SetActive(isShowing);
        }
        if (Input.GetKeyDown(KeyCode.Return) && gameController.gameOver)
        {
            gameController.gameOver = false;
            isShowing = false;
            menu.SetActive(isShowing);
            //gameController.gameOverText.text = "";
            
        }
	}
}
