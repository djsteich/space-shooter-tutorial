﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DestroyByContact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue; //how much killing these objects is worth in points
    private GameController gameController; //variable to hold our reference to the GameController instance
    private int enemyHealth;

    void OnTriggerEnter(Collider other)
    {
        PlayerController PC = other.gameObject.GetComponent<PlayerController>(); //explain logic/what's going on here || creating an instance?

        if (other.tag == "Boundary")
        {
            return; //synonymous with passing over the if statement
        }

        if (other.tag == "Enemy")
        {
            return;
        }
        
        
        if(other.tag == "Player" && PlayerController.health >=1)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            PlayerController.health -= 1;
            PC.transform.DOShakePosition(.5f, new Vector3(2,0,2),20, 45, false, true);
            
            //Debug.Log(PlayerController.health);
            gameController.UpdateHealth();
            Destroy(gameObject);
            if (PlayerController.health == 0)
            {
                Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
                gameController.GameOver();
                //Destroy(other.gameObject); //destroy other object
                Destroy(gameObject); //destroy self
            }
               
        }

        if (other.tag == "Projectile" && gameController.gameOver ==false)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(other.gameObject); //destroy projectile
            enemyHealth -= PlayerController.persistentDamage; 
            if (enemyHealth <=0)
            {
                gameController.AddScore(scoreValue);
                Destroy(other.gameObject); //destroy projectile object
                Destroy(gameObject); //destroy self
            }
           
        }

        if (other.tag == "Missile" & gameController.gameOver ==false)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            //Destroy(other.gameObject);
            enemyHealth -= 3;
            if (enemyHealth <= 0)
            {
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
            
        }


    }
    void Start ()
    {
        enemyHealth = 3;
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
        if (gameControllerObject == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (gameController.gameOver)
        {
            Destroy(gameObject);
        }
	}
}
